import collections
import time


filename = 'voina-i-mir.txt'
pattern = '[^a-zA-Zа-яА-Я]'


def timerfunc(func):
    """
    Декоратор для определения времени работы функции
    """

    def function_timer(*args, **kwargs):
        """
        врапер
        """
        start = time.time()
        value = func(*args, **kwargs)
        end = time.time()
        runtime = end - start
        msg = "Время работы функции {func} : {time} секунд"
        print(msg.format(func=func.__name__,
                         time=runtime))
        return value

    return function_timer



@timerfunc
def read_file_to_base():
    import re
    reg = re.compile(pattern)
    #компиляция регулярного выражения для нормализации строки
    file = open(filename, 'rt')
    #открытие файла
    __base = collections.Counter()
    #класс счетчика
    for line in file.readlines():
        for key in reg.sub(' ', line).split():
            __base[key.lower()] += 1

    file.close()
    return __base

@timerfunc
def read_from_base(keyboard, __base):
    list_with_keys = []
    for key in __base.keys():
        if keyboard in key:
            list_with_keys.append([key, __base[key]])
    list_with_keys.sort(key=lambda x: x[-1])
    list_with_keys.reverse()
    return list_with_keys[0:20]
    #вывод среза из первых 20ти элементов сортированного массива


def start():
    base = read_file_to_base()
    print('База загружена')
    #print(read_from_base('привет', base))
    #print(read_from_base('вой', base))
    while True:
        print('Введите слово \ часть слова для поиска:')
        keyboard = str(input()).lower()
        if len(keyboard) < 2:
            print('ошибка: менее 2х символов')
            continue

        for line in read_from_base(keyboard, base):
            print(f"    {str(line[0]).ljust(20)}   |   {str(line[-1]).ljust(4)}")



if __name__ == '__main__':
    start()
